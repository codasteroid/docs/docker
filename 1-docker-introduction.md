## Docker

### What is Docker?

Docker is a container management software. It allows you to package an application and its dependencies into a standarized unit called a **Container** (_Containerization_). This container can then be easily moved between different environments, ensuring consistent behavior regardless of where they are deployed.

#### What is a Docker Container?

**Container** is a lightweight, isolated, standalone, and executable package that contains everything needed to run an application, including code, runtime, system tools, system libraries, and settings.

For example, in a typical web application architecture, you might containerize different components such as the frontend, backend, and database into seperate containers. Each container can be independently managed, scaled, and updated without affecting other containers running on the same host.

#### What is a Docker Image?

**Image** is a read-only template used to create a container (A blueprint of a container). It's a snapshot of your container, it mades up of layers, you can have a base layer (Ubuntu, Debian), another layer (software files), then another layer (dependencies) and so on. When you build it up, you get the container.

Images are built using a **Dockerfile**, which a text file that contains instructions for building a Docker image.

#### What is Docker Compose?

**Docker Compose** is a tool for defining and running multi-container Docker applications. It uses a YAML file to configure the application's services, networks, and volumes, making it easy to manage complexe applictions with multiple containers.

#### Benefits

1. Simplicity: Easy install, I can handle to you the image and you can run it easily and have the same containers running as me.

2. Collaboration: Multiple developers can work on it, without worring having differences in dependencies, and binaries, etc.

3. Flexibilty: You can buid it and put in it whatever you want.

4. Totality: If you give me wordpress site, I might have a different version of mysql or php, or different version of Wordpress. Tje good thing about a container, is that it contains everything you need to run the application, whe nI handled to you, it has everything you to get the application up and running. 

#### VM vs Container 